let http = require("http");

const PORT = 3000;


let directory = [

	{
		"name":"Brandon",
		"email":"brandon@mail.com"
	},
	{
		"name": "Robert",
		"email": "robert@mail.com"
	}

];

http.createServer( (req,res) => {
// retrieve our documents
	// url = /users
	// method = GET
	// response = array of objects

	if(req.url === "/users" && req.method === "GET"){

		res.writeHead(200, {"Content-Type": "application/json"})
		res.end(JSON.stringify(directory));

	}
	if(req.url === "/users" && req.method == "POST"){
		
		let reqBody = "";

		req.on("data", (data) => {

			reqBody += data; // continue lang ang pag add

		});

		req.on("end", () => {

			reqBody = JSON.parse(reqBody);
			
			console.log(reqBody);
			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}

			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, 
				{"Content-Type": "application/json"}
			);
			res.write(JSON.stringify(directory));
			res.end();
		});
		
	}


} ).listen(PORT);
console.log(`Server is now connected to port ${PORT}`)