let http = require("http");
const PORT = 3000;
http.createServer( (req, res) => {
	//uri/endpoint = resource
	// http methods
	// body

	console.log(req);

	if(req.url === "/register" && req.method === "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Welcome to my page");
	}else if(req.url === "/profile" && req.method == "POST"){
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Welcome to my page POST");
	}else{
		res.writeHead(404, {"Content-Type": "text/plain"})
		res.end(`Sorry, request cannot be completed.`);
	}

} ).listen(PORT);

console.log(`Server is now connected to port ${PORT}`)